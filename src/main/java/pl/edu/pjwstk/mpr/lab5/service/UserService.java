package pl.edu.pjwstk.mpr.lab5.service;

import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
    	return users.stream()
    			.filter(user -> (user.getPersonDetails().getAddresses().size() > 1))
    			.collect(Collectors.toList());
    }

    public static Person findOldestPerson(List<User> users) {
        return users.stream()
        		.map(User::getPersonDetails)
        		.max(Comparator.comparing(Person::getAge))
        		.get();
    }

    public static User findUserWithLongestUsername(List<User> users) {
        return users.stream()
        		.sorted((user1, user2) -> Integer.compare(user1.getName().length(), user2.getName().length()))
        		.findFirst()
        		.get();
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        return users.stream()
        		.map(User::getPersonDetails)
        		.filter(person -> person.getAge() >= 18)
        		.map(person -> person.getName() + " " + person.getSurname())
        		.collect(Collectors.joining(", "));
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
        return users.stream()
        		.map(User::getPersonDetails)
        		.filter(person -> person.getName().charAt(0) == 'A')
        		.map(Person::getRole)
        		.map(Role::getPermissions)
        		.flatMap(List::stream)
        		.map(Permission::getName)
        		.collect(Collectors.toList());
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
        users.stream()
        	.map(User::getPersonDetails)
        	.filter(person -> person.getSurname().charAt(0) == 'S')
        	.map(Person::getRole)
        	.map(Role::getPermissions)
        	.flatMap(List::stream)
        	.map(Permission::getName)
        	.map(permissionName -> permissionName.toUpperCase())
        	.forEach(permissionName -> System.out.println(permissionName));
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
        return users.stream()
        		.collect(Collectors.groupingBy(user -> user.getPersonDetails().getRole()));
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
        return users.stream()
        		.collect(Collectors.groupingBy(user -> user.getPersonDetails().getAge() >= 18));
    }
}
