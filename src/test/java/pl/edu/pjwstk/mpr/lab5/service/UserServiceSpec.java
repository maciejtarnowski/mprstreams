package pl.edu.pjwstk.mpr.lab5.service;

import com.mscharhag.oleaster.runner.OleasterRunner;
import pl.edu.pjwstk.mpr.lab5.domain.User;
import pl.edu.pjwstk.mpr.lab5.domain.Address;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import org.junit.runner.RunWith;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import static com.mscharhag.oleaster.matcher.Matchers.*;

@RunWith(OleasterRunner.class)
public class UserServiceSpec {
	private UserService userService;
	private List<User> users;
{
	beforeEach(() -> {
		userService = new UserService();
		
	});
}
	private List<User> getTestUsers() {
		Permission permissionA = getPermission("A");
		Permission permissionB = getPermission("B");
		Permission permissionC = getPermission("C");
		Role roleAdmin = getRole("Admin", Arrays.asList(permissionA, permissionB, permissionC));
		List<Address> addresses = getAddresses();
		List<Permission> permissions = getPermissions();
		List<Role> roles = getRoles(permissions);
		
	}
	
	private List<Address> getAddresses() {
		return getCollection(5, (i) -> getAddress("Street" + i.toString(), i + 3, i + 1, "City" + i.toString(), "Postcode" + i.toString(), "Country" + i.toString()));
	}
	
	private Address getAddress(String streetName, Integer houseNumber, Integer flatNumber, String city, String postCode, String country) {
		return new Address()
				.setStreetName(streetName)
				.setHouseNumber(houseNumber)
				.setFlatNumber(flatNumber)
				.setCity(city)
				.setPostCode(postCode)
				.setCountry(country);
	}
	
	private List<Permission> getPermissions() {
		return getCollection(5, (i) -> getPermission("Permission" + i.toString()));
	}
	
	private Permission getPermission(String name) {
		return new Permission().setName(name);
	}
	
	private List<Role> getRoles(List<Permission> permissions) {
		return getCollection(5, (i) -> getRole("Role" + i.toString(), new ArrayList<Permission>()));
	}
	
	private Role getRole(String name, List<Permission> permissions) {
		return new Role()
				.setName(name)
				.setPermissions(permissions);
	}
	
	private <T> List<T> getCollection(Integer quantity, Function<Integer, T> generator) {
		List<T> collection = new ArrayList<T>();
		for (Integer i = 0; i < 10; i++) {
			collection.add(generator.apply(i));
		}
		return collection;
	}
}
